<%-- 
    Document   : stickyFooter
    Created on : Jun 14, 2023, 10:41:44 PM
    Author     : Acer
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <footer class="sticky-footer">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Copyright © H_Đ_T 2023</span>
                </div>
            </div>
        </footer>
    </body>
</html>
