/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package biz;

import java.util.List;

/**
 *
 * @author Acer
 */
public interface IGenericLogic<T> {
    List<T> findAll();
    
    int insertToDb(T t);
    
    void updateToDb(T t);
    
    void deleteBook(T t);
}
