/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package biz.impl;

import biz.IGenericLogic;
import constant.CommonConst;
import dal.impl.AccountDAO;
import entity.Account;
import entity.Parameter;
import java.sql.Types;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.TrippleDesASCUtils;

/**
 *
 * @author Acer
 */
public class AccountLogic implements IGenericLogic<Account> {

    AccountDAO dao;

    public AccountLogic() {
        dao = new AccountDAO();
    }

    @Override
    public List<Account> findAll() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int insertToDb(Account t) {
        //check username exist
        if (dao.findByUsername(t) != null) {
            throw new IllegalArgumentException("Username Exist !");
        }
        //check email exist
        if (dao.findByEmail(t) != null) {
            throw new IllegalArgumentException("Email Exist !");

        }
        //insert
        try {
            //insert
            t.setPassword(TrippleDesASCUtils.encrypt(t.getPassword()));
        } catch (Exception ex) {
            try {
                throw new Exception();
            } catch (Exception ex1) {
                Logger.getLogger(AccountLogic.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        return dao.insertToDb(t);
    }

    @Override
    public void updateToDb(Account t) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void deleteBook(Account t) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public Account findAccount(Account account, String optionFind) {
        Account accountFound = null;
//        account.setPassword(TrippleDesASCUtils.encrypt(account.getPassword()));
        switch (optionFind) {
            case CommonConst.FIND_ACCOUNT_BY_EMAIL:

                break;
            case CommonConst.FIND_ACCOUNT_BY_USERNAME_PASSWORD:
                accountFound = dao.findByUsernamePassword(account);
                break;

        }
        return accountFound;
    }

    public void updateIsVerify(int accountId) {
        dao.updateIsVerify(accountId);
    }

    public void updateAddress(String username, String address) {
        String sql = "UPDATE [dbo].[Account]\n"
                + "   SET \n"
                + "      [address] = ?\n"
                + "      \n"
                + " WHERE username = ?";
        dao.update(sql, new Parameter(address, Types.NVARCHAR),
                new Parameter(username, Types.NVARCHAR));

    }

}
