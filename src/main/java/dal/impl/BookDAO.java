/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal.impl;

import constant.CommonConst;
import dal.DBContext;
import dal.IGenericDAO;
import entity.Book;
import entity.Parameter;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import javax.script.ScriptEngine;
import mapper.impl.BookMapper;

/**
 *
 * @author Acer
 */
public class BookDAO extends DBContext<Book> implements IGenericDAO<Book> {

    @Override
    public List<Book> findAll() {
        String sql = "select * from Book";
        List<Book> list = query(sql, new BookMapper());
        return list;
    }

    @Override
    public Book findOneById(int id) {
        String sql = "select * from book where id = ?";
        List<Book> list = query(sql, new BookMapper(), new Parameter(id, Types.INTEGER));
        return list.isEmpty() ? null : list.get(0);
    }

    @Override
    public int insertToDb(Book t) {
        String s = "hoa hoa ";
        String price = "10";
         String sql = "INSERT INTO [Book]\n"
                + "           ([name]\n"
                + "           ,[description]\n"
                + "           ,[author]\n"
                + "           ,[price]\n"
                + "           ,[quantity]\n"
                + "           ,[image]\n"
                + "           ,[categoryId])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)";
        int id = insert(sql, new Parameter(s, Types.NVARCHAR),
             new Parameter(t.getDescription(), Types.NVARCHAR),
             new Parameter(t.getAuthor(), Types.NVARCHAR),
             new Parameter(price, Types.FLOAT),
             new Parameter(t.getQuantity(), Types.INTEGER),
             new Parameter(t.getImage(), Types.NVARCHAR),
             new Parameter(t.getCategoryId(), Types.INTEGER));
        
        return id;
    }

    @Override
    public void updateToDb(Book t) {
        String sql = "UPDATE [Book]\n"
                + "   SET [name] = ?\n"
                + "      ,[description] = ?\n"
                + "      ,[author] = ?\n"
                + "      ,[price] = ?\n"
                + "      ,[quantity] = ?\n"
                + "      ,[image] = ?\n"
                + "      ,[categoryId] = ?\n"
                + " WHERE id = ?";
        update(sql,
                new Parameter(t.getName(), Types.NVARCHAR),
                new Parameter(t.getDescription(), Types.NVARCHAR),
                new Parameter(t.getAuthor(), Types.NVARCHAR),
                new Parameter(t.getPrice(), Types.FLOAT),
                new Parameter(t.getQuantity(), Types.INTEGER),
                new Parameter(t.getImage(), Types.NVARCHAR),
                new Parameter(t.getCategoryId(), Types.INTEGER),
                new Parameter(t.getId(), Types.INTEGER)
        );
    }

    @Override
    public void delete(Book t) {
        String sql = "DELETE FROM [dbo].[Book]\n" +
                     " WHERE id = ?";
        update(sql,
                new Parameter(t.getId(), Types.INTEGER));
    }

    public int findTotalRecord(String keyword) {
        String sql = "SELECT COUNT (*) FROM Book\n"
                + "where name like ?";
        return findTotalRecord(sql, new Parameter("%" + keyword + "%", Types.NVARCHAR));
    }

    public List<Book> findBooksByPage(int page, String keyword) {
        String sql = "select * \n"
                + "from Book\n"
                + "where name like ?\n"
                + "order by id\n"
                + "offset (? - 1) * ?\n"
                + "rows fetch next ?\n"
                + "rows only";

        List<Book> listByCurrentPage = query(sql, new BookMapper(),
                new Parameter("%" + keyword + "%", Types.NVARCHAR),
                new Parameter(page, Types.INTEGER),
                new Parameter(CommonConst.BOOK_RECORD_PER_PAGE, Types.INTEGER),
                new Parameter(CommonConst.BOOK_RECORD_PER_PAGE, Types.INTEGER));
        return listByCurrentPage;
    }

    public static void main(String[] args) {
        BookDAO dao = new BookDAO();
        for (Book book : dao.findBooksByPage(1, "h")) {
            System.out.println(book);
        }
    }

    public int findTotalRecordByCateId(int categoryId) {
        String sql = "select count(*)\n"
                + "from Book\n"
                + "where categoryId = ?";
        return findTotalRecord(sql, new Parameter(categoryId, Types.INTEGER));
    }

    public List<Book> findBooksByCategory(int page, int categoryId) {
        String sql = "select * \n"
                + "from Book\n"
                + "where categoryId = ?\n"
                + "order by id\n"
                + "offset (? - 1) * ?\n"
                + "rows fetch next ?\n"
                + "rows only";
        
        List<Book> listBooksByCategory = query(sql, new BookMapper(),
                new Parameter(categoryId, Types.INTEGER),
                new Parameter(page, Types.INTEGER),
                new Parameter(CommonConst.BOOK_RECORD_PER_PAGE, Types.INTEGER),
                new Parameter(CommonConst.BOOK_RECORD_PER_PAGE, Types.INTEGER));
        return listBooksByCategory;
    }

    public HashMap<Integer, Book> findBooksByAccountId(int id) {
        String sql = "select b.*\n"
                + "from Account as a, Book as b, [Order] as o, OrderDetails as od\n"
                + "where b.id = od.bookId and od.orderId = o.id and o.accountId = a.id\n"
                + "and a.id = ?";
        List<Book> list = query(sql, new BookMapper(),
                new Parameter(id, Types.INTEGER)
        );
        HashMap<Integer, Book> hashMap = new HashMap<>();
        // Iterate through the ArrayList and add elements to the HashMap
        for (Book book : list) {
            hashMap.put(book.getId(), book);
        }
        return hashMap;
    }
}
