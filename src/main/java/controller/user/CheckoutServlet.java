/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.user;

import dal.impl.OrderDAO;
import dal.impl.OrderDetailsDAO;
import entity.Account;
import entity.ItemCart;
import entity.Order;
import entity.OrderDetails;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import utils.CommonUtils;

/**
 *
 * @author Acer
 */
public class CheckoutServlet extends HttpServlet {

        @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("view/user/cart/checkout.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        OrderDAO orderDAO = new OrderDAO();
        OrderDetailsDAO orderDetailsDAO = new OrderDetailsDAO();

        //get information ( note)
        String note = request.getParameter("note");
        //get account on session
        Account account = (Account) session.getAttribute("account");
        //get cart on session
        HashMap<Integer, ItemCart> cart = (HashMap<Integer, ItemCart>) session.getAttribute("cart");
        //calculate total amount of cart
        float amount = calculateTotalAmount(cart);

        //create order and insert to DB ( return id of order)
        Order order = Order.builder().
                amount(amount).
                description(note).
                createAt(CommonUtils.getCurrentTimestamp()).
                accountId(account.getId()).
                build();
        int orderId = orderDAO.insertToDb(order);
        //create order details for each item in cart and insert to DB
        for (Map.Entry<Integer, ItemCart> entry : cart.entrySet()) {
            int key = entry.getKey();
            ItemCart val = entry.getValue();
            OrderDetails orderDetails = OrderDetails.builder().
                    quantity(val.getQuantity()).
                    bookId(key).
                    orderId(orderId).
                    build();
            orderDetailsDAO.insertToDb(orderDetails);
        }

        //go to dashboard
        response.sendRedirect("dashboard");
    }

    private float calculateTotalAmount(HashMap<Integer, ItemCart> cart) {
        float total = 0;
        for (Map.Entry<Integer, ItemCart> entry : cart.entrySet()) {
            int key = entry.getKey();
            ItemCart val = entry.getValue();
            total += val.getAmount();
        }
        return total;
    }

}
