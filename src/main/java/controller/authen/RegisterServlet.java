/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.authen;

import biz.impl.AccountLogic;
import biz.impl.EmailLogLogic;
import biz.impl.VerifyRequestLogic;
import constant.CommonConst;
import entity.Account;
import entity.EmailLog;
import entity.VerifyRequest;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import utils.CommonUtils;
import utils.EmailUtils;
import utils.TrippleDesASCUtils;

/**
 *
 * @author Acer
 */
public class RegisterServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet RegisterServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet RegisterServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("view/common/authen/register.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       AccountLogic accountLogic = new AccountLogic();
        EmailLogLogic emailLogLogic = new EmailLogLogic();
        VerifyRequestLogic verifyRequestLogic = new VerifyRequestLogic();
        
        //get information from form
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String email = request.getParameter("email");

        //create instance account
        Account account = Account.builder().
                username(username).
                password(password).
                email(email).
                isVerify(false).
                roleId(CommonConst.ROLE_USER).
                build();
        
        try {
            int id = accountLogic.insertToDb(account);
            //generate token
            String token = TrippleDesASCUtils.encrypt(CommonUtils.generateRandomUUID());

            //get link
            String link = request.getScheme() + "://" + request.getServerName() + ":"
                    + request.getServerPort() + request.getContextPath() + "/verify?token=" + token;

            //send mail
            boolean isSendMailSuccess = emailLogLogic.sendMail(CommonConst.TYPE_SEND_MAIL_REGISTER,
                    EmailUtils.getContentMailRegister(link), account.getEmail());
            
                        if (isSendMailSuccess) {
                EmailLog emailLog = EmailLog.builder().
                        to(account.getEmail()).
                        subject("REGISTER").
                        content(EmailUtils.getContentMailRegister(link)).
                        build();
                //insert to db emailLog
                int emailLogId = emailLogLogic.insertToDb(emailLog);
                
                VerifyRequest verifyRequest = VerifyRequest.builder().
                        requestContent(token).
                        isVerify(false).
                        accountId(id).
                        emailLogId(emailLogId).
                        createAt(CommonUtils.getCurrentTimestamp()).
                        expired(CommonUtils.getTimestampExpired()).
                        build();
                        
                verifyRequestLogic.insertToDb(verifyRequest);
                
                request.setAttribute("mess", "XAC THUC THANH CONG");
                request.getRequestDispatcher("view/common/thanhcong.jsp").forward(request, response);
            }
            
        } catch (IllegalArgumentException e) {
            //set error
            request.setAttribute("error", e.getMessage());
            //redirect register
            request.getRequestDispatcher("view/common/authen/register.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
