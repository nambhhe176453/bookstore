/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.admin;

import biz.impl.BookLogic;
import biz.impl.CategoryLogic;
import entity.Book;
import entity.Category;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Acer
 */
public class DashboardAminServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        BookLogic bookLogic = new BookLogic();
        CategoryLogic categoryLogic = new CategoryLogic();
        
        //get session
        HttpSession session = request.getSession();
        //get list all book
        List<Book> listAllBooks = bookLogic.findAll();
        List<Category> listCategories = categoryLogic.findAll();
        //set attribute
        session.setAttribute("listBook", listAllBooks);
        session.setAttribute("listCategories", listCategories);
        request.getRequestDispatcher("../view/admin/dashboard/index.jsp")
                .forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

}
